/* ============
 * Actions for the home module
 * ============
 *
 */
import axios from '@/plugins/axios'

import {
  SET_HOME_STATE
} from './mutation-types'

export default {
  async getNewWorks ({ state, commit }) {
    const data = [] // await axios.get('works')

    commit(SET_HOME_STATE, {
      key: 'newWorks',
      value: data
    })
  }
}
