/* ============
 * Mutations for the home state
 * ============
 *
 * The mutations that are available on the
 * home state.
 */

import {
  SET_HOME_STATE
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_HOME_STATE] (state, { key, value = null }) {
    state[key] = value
  }
}
