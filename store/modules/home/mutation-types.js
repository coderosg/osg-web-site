/* ============
 * Mutation types for the home state
 * ============
 *
 */

export const SET_HOME_STATE = 'SET_HOME_STATE'

export default {
  SET_HOME_STATE
}
