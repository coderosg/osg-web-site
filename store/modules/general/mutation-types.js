/* ============
 * Mutation types for the general state
 * ============
 *
 */

export const SET_GENERAL_STATE = 'SET_GENERAL_STATE'

export default {
  SET_GENERAL_STATE
}
