/* ============
 * Mutations for the general state
 * ============
 *
 * The mutations that are available on the
 * general state.
 */

import {
  SET_GENERAL_STATE
} from './mutation-types'

/* eslint-disable no-param-reassign */
export default {
  [SET_GENERAL_STATE] (state, { key, value = null }) {
    state[key] = value
  }
}
