/* ============
 * Actions for the general module
 * ============
 *
 */

import {SET_GENERAL_STATE} from "./mutation-types"

export default {
  setLang ({ state, commit }, payload) {
    commit(SET_GENERAL_STATE, {
      key: 'locale',
      value: payload
    })
  }
}
