module.exports = {
  /*
  ** Router config
  */
  router: {
    middleware: 'i18n'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'osg.uz',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'OSG web site' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Integrated modules
  */
  modules: [
    'nuxt-vuex-router-sync'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: [
      '~plugins/axios',
      'element-ui'
    ]
    // transpile: [
    //   'vue-ramda'
    // ]
  },
  /*
  ** Cascading Style Sheets
  */
  css: [
    // font-awesome
    { src: '@fortawesome/fontawesome-free/css/all.css', lang: 'css' },
    { src: '@/scss/index.scss', lang: 'scss' },
  ],
  /*
  ** Plugins
  */
  plugins: [
    '@/plugins/vue-i18n',
    '@/plugins/element-ui',
    '@/plugins/vue-lodash'
  ]
}

