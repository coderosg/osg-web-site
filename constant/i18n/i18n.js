import EN from './en'
import RU from './ru'
import UZ from './uz'

export const TRANSLATES = {
  'en': EN,
  'ru': RU,
  'uz': UZ
}

export const LANGUAGES = ['en', 'ru', 'uz']

export const LANGUAGES_NAMES = [{
    lang: 'en',
    label: 'English'
  },
  {
    lang: 'ru',
    label: 'Russian'
  },
  {
    lang: 'uz',
    label: 'O`zbekcha'
  }
]

export default TRANSLATES
