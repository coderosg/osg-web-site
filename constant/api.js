export const API_HOST = 'https://api.osg.uz'
export const API_ROOT = 'api'
export const API_VERSION = 'v0'
export const API_URL = `${API_HOST}/${API_ROOT}/${API_VERSION}`
