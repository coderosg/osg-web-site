/* ============
 * Vue I18n
 * ============
 *
 * Vue I18n is internationalization plugin for Vue.js
 *
 * http://kazupon.github.io/vue-i18n/
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'

import {DEFAULT_LANG} from '@/constant/settings'
import messages from '@/constant/i18n/index'

Vue.use(VueI18n)

export default ({ app, store }) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: DEFAULT_LANG,
    // silentTranslationWarn: true,
    messages
  })

  app.i18n.path = (link) => {
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`
    }

    return `/${app.i18n.locale}/${link}`
  }
}
