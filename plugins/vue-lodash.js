/* ============
 * Vue lodash
 * ============
 *
 * Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc.
 *
 * https://github.com/Ewocker/vue-lodash
 */

import Vue from 'vue'
import VueLodash from 'vue-lodash'

const options = { name: '$lodash' } // customize the way you want to call it

Vue.use(VueLodash, options) // options is optional
