import Vue from 'vue'
import axios from 'axios'
import i18n from '@/plugins/vue-i18n'
import { Message } from 'element-ui'
import {API_URL} from "../constant/api"

// Set base url
// axios.defaults.baseURL = API_URL

// Response interceptor
axios.interceptors.response.use(
  response => response,
  error => {
    let status = 400
    if (error.response === undefined) status = error.response

    if (status >= 500) {
      Message({
        type: 'error',
        message:i18n.t('Something goes wrong')
      });
    }

    return Promise.reject(error)
  }
)

// Bind Axios to Vue
Vue.$http = axios
if (process.client) {
  Object.defineProperty(Vue.prototype, '$http', {
    get() {
      return axios
    }
  })
}

export default axios

