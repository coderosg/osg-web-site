/* ============
 * Element UI
 * ============
 *
 * Element, a Vue 2.0 based component library for developers, designers and product managers
 *
 * https://element.eleme.io/#/en-US
 */
import Vue from 'vue'
import ElementUI from 'element-ui'

Vue.use(ElementUI, { locale: 'en' })
